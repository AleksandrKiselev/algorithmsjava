package ru.kiselev.algorithms.divide_and_conquer;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MaxSubArrayFinderTest {

    private final MaxSubArrayFinder finder = new MaxSubArrayFinder();

    @DataProvider
    public Object[][] testData() {
        return new Object[][] {
                {new int[]{}, new MaxSubArrayFinder.SubArrayInfo(0,0,0)},
                {new int[]{1}, new MaxSubArrayFinder.SubArrayInfo(0,0,1)},
                {new int[]{1,2,3}, new MaxSubArrayFinder.SubArrayInfo(0,2,6)},
                {new int[]{1,2,3,4}, new MaxSubArrayFinder.SubArrayInfo(0,3,10)},
                {new int[]{3,2,1}, new MaxSubArrayFinder.SubArrayInfo(0,2,6)},
                {new int[]{-1,-1,-1,-1}, new MaxSubArrayFinder.SubArrayInfo(1,1,-1)},
                {new int[]{-1,45,3,-1,1,2,3,56,-100,78,0,0,32,-1,-2,100}, new MaxSubArrayFinder.SubArrayInfo(9,12,110)},
        };
    }

    @Test(dataProvider = "testData")
    public void testFindMaxSubArray(int[] input, MaxSubArrayFinder.SubArrayInfo output) {
        Assert.assertEquals(finder.run(input), output);
    }
}
