package ru.kiselev.algorithms.sort;


import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SortTest {

    @DataProvider
    public Object[][] sortData() {
        return new Object[][] {
                {new int[] {}, new int[] {}},
                {new int[] {1}, new int[] {1}},
                {new int[] {1,1,1}, new int[] {1,1,1}},
                {new int[] {1,2,3}, new int[] {1,2,3}},
                {new int[] {3,2,1}, new int[] {1,2,3}},
                {new int[] {3,1,2}, new int[] {1,2,3}},
                {new int[] {0,10,4,14,100}, new int[] {0,4,10,14,100}},
                {
                    new int[] {0,10,-32,4,-123,1000,1,1,4,0,43,2,89,54,34,7,-87,-1,-11},
                    new int[] {-123,-87,-32,-11,-1,0,0,1,1,2,4,4,7,10,34,43,54,89,1000}
                }
        };
    }

    @Test(dataProvider = "sortData")
    public void testInsertionSort(int[] orig, int[] sorted) {
        var orig_copy = Arrays.copyOf(orig, orig.length);
        InsertionSort.run(orig_copy);
        Assert.assertEquals(orig_copy, sorted);
    }

    @Test(dataProvider = "sortData")
    public void testSelectionSort(int[] orig, int[] sorted) {
        var orig_copy = Arrays.copyOf(orig, orig.length);
        SelectionSort.run(orig_copy);
        Assert.assertEquals(orig_copy, sorted);
    }

    @Test(dataProvider = "sortData")
    public void testMergeSort(int[] orig, int[] sorted) {
        var orig_copy = Arrays.copyOf(orig, orig.length);
        MergeSort.run(orig_copy);
        Assert.assertEquals(orig_copy, sorted);
    }

    @Test(dataProvider = "sortData")
    public void testBubbleSort(int[] orig, int[] sorted) {
        var orig_copy = Arrays.copyOf(orig, orig.length);
        BubbleSort.run(orig_copy);
        Assert.assertEquals(orig_copy, sorted);
    }

    @Test(dataProvider = "sortData")
    public void testHeapSort(int[] orig, int[] sorted) {
        var orig_list = IntStream.of(orig).boxed().collect(Collectors.toList());
        var sorted_list = IntStream.of(sorted).boxed().collect(Collectors.toList());
        HeapSort.run(orig_list);
        Assert.assertEquals(orig_list, sorted_list);
    }

    @Test(dataProvider = "sortData")
    public void testQuicksort(int[] orig, int[] sorted) {
        var orig_copy = Arrays.copyOf(orig, orig.length);
        Quicksort.run(orig_copy);
        Assert.assertEquals(orig_copy, sorted);
    }

    @Test(dataProvider = "sortData")
    public void testCountingSort(int[] orig, int[] sorted) {
        var result = CountingSort.run(orig);
        Assert.assertEquals(result, sorted);
    }

    @Test(dataProvider = "sortData")
    public void testRadixSort(int[] orig, int[] sorted) {
        var orig_copy = Arrays.copyOf(orig, orig.length);
        RadixSort.run(orig_copy, 4);
        Assert.assertEquals(orig_copy, sorted);
    }
}
