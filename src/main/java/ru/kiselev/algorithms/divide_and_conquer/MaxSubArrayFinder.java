package ru.kiselev.algorithms.divide_and_conquer;

public class MaxSubArrayFinder {
    public static class SubArrayInfo {
        public int start_index;
        public int end_index;
        public int sum;

        public SubArrayInfo(int start_index, int end_index, int sum) {
            this.start_index = start_index;
            this.end_index = end_index;
            this.sum = sum;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SubArrayInfo that = (SubArrayInfo) o;

            if (start_index != that.start_index) return false;
            if (end_index != that.end_index) return false;
            return sum == that.sum;
        }

        @Override
        public String toString() {
            return "SubArrayInfo{" +
                    "start_index=" + start_index +
                    ", end_index=" + end_index +
                    ", sum=" + sum +
                    '}';
        }
    }

    public SubArrayInfo run(int[] array) {
        return findRecursive(array,0, array.length - 1);
    }

    private SubArrayInfo findRecursive(int[] array, int start_index, int end_index) {
        if (array.length == 0)
            return new SubArrayInfo(0,0, 0);
        else if (start_index == end_index)
            return new SubArrayInfo(0,0, array[0]);
        else {
            int mid_index = (start_index + end_index) / 2;
            var left = findRecursive(array, start_index, mid_index);
            var right = findRecursive(array, mid_index + 1, end_index);

            int sum = array[mid_index];

            int left_index = mid_index;
            for (int temp_index = left_index - 1; temp_index >= start_index; temp_index--) {
                if (sum > sum + array[temp_index])
                    break;
                sum += array[temp_index];
                left_index = temp_index;
            }

            int right_index = mid_index;
            for (int temp_index = right_index + 1; temp_index <= end_index; temp_index++) {
                if (sum > sum + array[temp_index])
                    break;
                sum += array[temp_index];
                right_index = temp_index;
            }

            var curr = new SubArrayInfo(left_index, right_index, sum);

            if (left.sum > right.sum && left.sum > curr.sum)
                return left;
            if (right.sum > left.sum && right.sum > curr.sum)
                return right;
            return curr;
        }
    }
}
