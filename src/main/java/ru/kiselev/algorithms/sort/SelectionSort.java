package ru.kiselev.algorithms.sort;

public class SelectionSort {
    public static void run(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            SortUtils.swap(array, min, i);
        }
    }
}
