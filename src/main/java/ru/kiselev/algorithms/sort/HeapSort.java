package ru.kiselev.algorithms.sort;

import ru.kiselev.algorithms.structures.MaxHeap;

import java.util.List;

class SortableHeap extends MaxHeap<Integer> {
    public SortableHeap(List<Integer> nodes) {
        super(nodes);
    }

    protected void sort() {
        for (int i = nodes.size() - 1; i >= 1; i--) {
            swap(0, i);
            heapSize--;
            heapify(0);
        }
    }
}

public class HeapSort {
    public static void run(List<Integer> list) {
        final var heap = new SortableHeap(list);
        heap.sort();
    }
}
