package ru.kiselev.algorithms.sort;

public class CountingSort {
    static private int index(int value, int min) {
        return value - min;
    }

    static public int[] run(int[] array) {
        if (array.length == 0)
            return array;

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (final int value : array) {
            if (value < min)
                min = value;
            if (value > max)
                max = value;
        }

        final var count = max - min + 1;
        final var C = new int[count];
        for (final int value : array) {
            C[index(value, min)]++;
        }

        for (int i = 1; i < C.length; i++) {
            C[i] += C[i - 1];
        }

        final var result = new int[array.length];
        for (int i = array.length - 1; i >= 0; i--) {
            final var value = array[i];
            final var c_idx = index(value, min);
            final var r_idx = C[c_idx] - 1;
            result[r_idx] = value;
            C[c_idx]--;
        }

        return result;
    }
}
