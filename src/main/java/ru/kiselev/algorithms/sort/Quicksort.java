package ru.kiselev.algorithms.sort;

import java.util.Random;

public class Quicksort {

    private static int randomizedPartition(int[] array, int p, int r) {
        Random rand = new Random();
        int i = rand.nextInt((r - p) + 1) + p;
        SortUtils.swap(array, i, r);
        return partition(array, p, r);
    }

    private static int partition(int[] array, int p, int r) {
        int pivot = array[r];
        int i = p - 1;
        for (int j = p; j < r; j++) {
            if (array[j] <= pivot) {
                i++;
                SortUtils.swap(array, i, j);
            }
        }
        SortUtils.swap(array, i + 1, r);
        return i + 1;
    }

    private static void qsort(int[] array, int p, int r) {
        if (p < r) {
            int q = randomizedPartition(array, p, r);
            qsort(array, p, q - 1);
            qsort(array, q + 1, r);
        }
    }

    public static void run(int[] array) {
        qsort(array, 0, array.length - 1);
    }
}
