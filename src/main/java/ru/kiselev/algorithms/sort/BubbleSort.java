package ru.kiselev.algorithms.sort;

public class BubbleSort {
    public static void run(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = array.length - 1; j >= i + 1; j--) {
                if (array[j] < array[j - 1]) {
                    SortUtils.swap(array, j, j - 1);
                }
            }
        }
    }
}
