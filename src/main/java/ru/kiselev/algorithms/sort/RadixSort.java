package ru.kiselev.algorithms.sort;

public class RadixSort {
    private static final int[] POWERS_OF_10 =
            { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };
    private static int powerOfTen(int pow) {
        return POWERS_OF_10[pow];
    }

    private static final int NUMBER_OF_NEGATIVE_DIGITS = 9;
    private static final int NUMBER_OF_POSITIVE_DIGITS_AND_ZERO = 10;

    private static int digit(int number, int pos) {
        return number / powerOfTen(pos) % 10;
    }

    private static int index(int value, int pos) {
        return digit(value, pos) + NUMBER_OF_NEGATIVE_DIGITS;
    }

    private static int[] countingSort(int[] array, int pos) {
        var C = new int[NUMBER_OF_NEGATIVE_DIGITS + NUMBER_OF_POSITIVE_DIGITS_AND_ZERO];
        for (int value : array) {
            C[index(value, pos)]++;
        }
        for (int i = 1; i < C.length; i++) {
            C[i] += C[i - 1];
        }
        var result = new int[array.length];
        for (int i = array.length - 1; i >= 0; i--) {
            final var value = array[i];
            final var c_idx = index(value, pos);
            result[C[c_idx] - 1] = value;
            C[c_idx]--;
        }
        return result;
    }

    public static void run(int[] array, int d) {
        for (int i = 0; i < d; i++) {
            final var sorted = countingSort(array, i);
            System.arraycopy(sorted, 0, array, 0, sorted.length);
        }
    }
}
