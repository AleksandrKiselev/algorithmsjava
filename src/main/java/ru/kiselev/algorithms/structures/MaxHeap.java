package ru.kiselev.algorithms.structures;

import java.util.List;

public class MaxHeap<T extends Comparable<T>> {
    protected List<T> nodes;
    protected int heapSize;

    public MaxHeap(List<T> nodes) {
        build(nodes);
    }

    protected void checkEmpty() throws Exception {
        if (heapSize == 0)
            throw new Exception("heap is empty");
    }

    protected void checkIndex(int index) {
        if (0 > index || index >= heapSize) {
            throw new IndexOutOfBoundsException();
        }
    }

    protected T get(int index){
        checkIndex(index);
        return nodes.get(index);
    }

    public T max() throws Exception {
        checkEmpty();
        return nodes.get(0);
    }

    public T extractMax() throws Exception {
        checkEmpty();
        final var root = nodes.get(0);
        final var leaf = nodes.get(heapSize - 1);
        nodes.set(0, leaf);
        heapSize--;
        heapify(0);
        return root;
    }

    public void increaseValue(int index, T newValue) throws Exception {
        checkIndex(index);
        if (greaterThanValue(index, newValue))
            throw new Exception("new value is less than the previous");
        nodes.set(index, newValue);
        while (index > 0 && greaterThan(index, parent(index))) {
            swap(index, parent(index));
            index = parent(index);
        }
    }

    public void insert(T value) throws Exception {
        final var index = heapSize;
        heapSize++;
        if (index >= nodes.size()) {
            nodes.add(value);
        } else {
            nodes.set(index, value);
        }
        increaseValue(index, value);
    }


    protected int parent(int index) {
        return index >> 1;
    }

    protected int left(int index) {
        return index << 1;
    }

    protected int right(int index) {
        return (index << 1) + 1;
    }

    protected void swap(int index1, int index2) {
        if (index1 != index2) {
            final T tempVal = nodes.get(index2);
            nodes.set(index2, nodes.get(index1));
            nodes.set(index1, tempVal);
        }
    }

    protected boolean greaterThan(int index1, int index2) {
        final T val1 = get(index1);
        final T val2 = get(index2);
        return val1.compareTo(val2) > 0;
    }

    protected boolean greaterThanValue(int index, T value) {
        final T val = get(index);
        return val.compareTo(value) > 0;
    }

    protected void heapify(int index) {
        if (0 <= index && index < heapSize) {
            int largestIndex = index;

            final int leftIndex = left(index);
            if (leftIndex < heapSize) {
                if (greaterThan(leftIndex, index)) {
                    largestIndex = leftIndex;
                }
            }

            final int rightIndex = right(index);
            if (rightIndex < heapSize) {
                if (greaterThan(rightIndex, largestIndex)) {
                    largestIndex = rightIndex;
                }
            }

            if (largestIndex != index) {
                swap(index, largestIndex);
                heapify(largestIndex);
            }
        }
    }

    protected void build(List<T> list) {
        nodes = list;
        heapSize = nodes.size();
        for (int i = (int)Math.floor(heapSize / 2.0); i >= 0; i--) {
            heapify(i);
        }
    }
}
